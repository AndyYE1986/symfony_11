<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'required'   => true,
               'constraints' => new Length([
                        'min' => 1,
                        'minMessage' => 'Your content should be at least {{ limit }} characters',
                        'max' => 255])
            ])
            ->add('content', TextareaType::class, [
                'required'   => true,
                'constraints' =>
                    new Length([
                        'min' => 1,
                        'minMessage' => 'Your content should be at least {{ limit }} characters',
                        'max' => 4096,
                    ])
            ])
            ->add('img', FileType::class, [
                                'required' => true,
                                'mapped' => false,
                                'constraints' => new Image(['maxSize' => '9024k'])
                                    ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
