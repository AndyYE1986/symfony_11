<?php

namespace App\Services;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Psr\Log\LoggerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;


class RegisterService
{
    protected UserPasswordHasherInterface $passwordHasher;
    protected EntityManagerInterface      $em;
    protected LoggerInterface             $logger;

    public function __construct(UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->passwordHasher = $passwordHasher;
        $this->em = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @throws Exception
     */
    public function register(User $user, Request $request): bool
    {
        try {
            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $request->request->all()['user']['password']['first']
            );
            // Encode the new users password
            $user->setPassword($hashedPassword);

            // Set their role
            $user->setRoles(['ROLE_USER']);

            // Save
            $this->em->persist($user);
            $this->em->flush();

        } catch (Exception $e) {
            $this->logger->error('error register user. ' . $e->getMessage());
            throw new Exception('error register user');
        }

        return true;
    }
}