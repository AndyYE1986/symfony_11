<?php

namespace App\Services;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Psr\Log\LoggerInterface;


class ArticleService
{
    protected EntityManagerInterface $em;
    protected Security $security;
    protected LoggerInterface $logger;
    protected PaginatorInterface $paginator;

    public function __construct(EntityManagerInterface $entityManager, Security $security, LoggerInterface $logger, PaginatorInterface $paginator)
    {
        $this->em = $entityManager;
        $this->security = $security;
        $this->logger = $logger;
        $this->paginator = $paginator;
    }

    /**
     * @throws Exception
     */
    public function saveArticle(array $data): bool
    {
        try {
            $article = new Article();

            $userId = $this->security->getUser()->getId();

            $user = $this->em->getRepository(User::class)->find($userId);

            $article->setTitle($data['title']);
            $article->setContent($data['content']);
            $article->setUser($user);
            $article->setImg('uploads/' . $data['img']);
            $article->setDatetime(new \DateTime('now'));

            // Save
            $this->em->persist($article);
            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->error('error save an article. ' . $e->getMessage());
            throw new Exception('error save an article');
        }

        return true;
    }

    public function getUserArticles(Request $request, int $perPage): \Knp\Component\Pager\Pagination\PaginationInterface
    {
        $user = $this->security->getUser();
        $dql   = "SELECT a FROM App\Entity\Article a WHERE a.user =" . $user->getId()." ORDER BY a.datetime DESC";

        $query = $this->em->createQuery($dql);

        return $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1), /*page number*/
            $perPage
        );
    }

    public function getArticles(Request $request, int $perPage): \Knp\Component\Pager\Pagination\PaginationInterface
    {
        $dql   = "SELECT a.id, a.title, a.content, a.datetime, a.img, u.name FROM App\Entity\Article a JOIN a.user u ORDER BY a.datetime DESC";
        $query = $this->em->createQuery($dql);

        return $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1), /*page number*/
            $perPage
        );
    }
}