<?php

namespace App\Services;

use App\Entity\Article;
use App\Entity\User;
use App\Interfaces\DownloadInterface;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Security\Core\Security;
use Psr\Log\LoggerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;


class DownloadService implements DownloadInterface
{
    protected EntityManagerInterface $em;
    protected Security               $security;
    protected LoggerInterface        $logger;
    protected SluggerInterface       $slugger;

    public function __construct(EntityManagerInterface $entityManager, Security $security, LoggerInterface $logger, SluggerInterface $slugger)
    {
        $this->em = $entityManager;
        $this->security = $security;
        $this->logger = $logger;
        $this->slugger = $slugger;
    }

    /**
     * @throws Exception
     */
    public function saveFile($file, string $path): string
    {
        try {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

            // Move the file to the directory where brochures are stored

            $file->move(
                $path,
                $newFilename);

        } catch (Exception $e) {
            $this->logger->error('error save an img of article. ' . $e->getMessage());
            throw new Exception('error save an img of article');
        }

        return $newFilename;
    }
}