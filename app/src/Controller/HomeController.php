<?php

namespace App\Controller;

use App\Services\ArticleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class HomeController extends AbstractController
{
    /**
     * @var ArticleService
     */
    private ArticleService $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    #[Route('/', name: 'home')]
    public function index(Request $request): Response
    {
        $pagination = $this->articleService->getArticles($request, $this->getParameter('per_page_home_page'));
//dd($pagination->count());
        return $this->render('home/home.html.twig', ['pagination' => $pagination, 'exist' => $pagination->count()]);
    }
}
