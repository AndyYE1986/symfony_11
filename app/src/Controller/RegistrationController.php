<?php

namespace App\Controller;

use App\Services\RegisterService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class RegistrationController extends AbstractController
{
    private RegisterService $registerService;

    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
    }

    #[Route('/register', name: 'register')]
    public function index(ValidatorInterface $validator, ManagerRegistry $doctrine, Request $request): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        $user = new User();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $this->registerService->register($user, $request);
            } catch (\Exception $e) {
                $this->addFlash(
                    'notice',
                    $e->getMessage()
                );

                return $this->redirectToRoute('register');
            }

            $this->addFlash(
                'notice',
                'Success registered. Please login now!'
            );

            return $this->redirectToRoute('app_login');
        }

        return $this->render('register/index.html.twig', [
            'form'   => $form->createView(),
            'errors' => $form->isSubmitted() ? $validator->validate($user) : []
        ]);
    }
}
