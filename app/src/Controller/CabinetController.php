<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Interfaces\DownloadInterface;
use App\Services\ArticleService;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;



class CabinetController extends AbstractController
{
    /**
     * @var ArticleService
     */
    private ArticleService $articleService;
    /**
     * @var DownloadInterface
     */
    private DownloadInterface $downloadService;

    public function __construct(ArticleService $articleService, DownloadInterface $downloadService)
    {
        $this->articleService = $articleService;
        $this->downloadService = $downloadService;
    }

    #[Route('/cabinet', name:'cabinet')]
    public function articles(Request $request): Response
    {
        $pagination = $this->articleService->getUserArticles($request, $this->getParameter('per_page'));
        return $this->render('cabinet/cabinet.html.twig', ['pagination' => $pagination]);
    }

    #[Route('/cabinet/article', name:'cabinet.article')]
    public function addArticle(ValidatorInterface $validator, Request $request): Response
    {
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $request->request->all()['article'];

            try {
                // save img
                $data['img'] = $this->downloadService->saveFile($form->get('img')->getData(), $this->getParameter('uploads_directory'));

                // save an article in db
                $this->articleService->saveArticle($data);
            } catch (\Exception $exception) {
                $this->addFlash('notice', $exception->getMessage());

                return $this->redirectToRoute('cabinet.article');
            }

            $this->addFlash('notice', 'article saved successfully');

            return $this->redirectToRoute('cabinet');
        }

        return $this->render('cabinet/article.html.twig', [
            'form' => $form->createView(),
            'errors' => $form->isSubmitted() ? $validator->validate($article) : []
        ]);
    }
}
