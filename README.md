docker-compose up -d --build

docker-compose exec php /bin/bash

mkdir ./public/uploads

chmod 0777 ./public/uploads

rm -rf vendor/*

composer install

php bin/console make:migration

php bin/console doctrine:migrations:migrate
